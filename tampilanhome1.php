<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION['MM_Username'] = NULL;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
	
  $logoutGoTo = "index.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="jquery-1.7.1.min.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistem Pakar</title>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="styles/style_admin.css" rel="stylesheet" type="text/css">
<style type="text/css">
body,td,th,tr {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #B7DDE8;
	background-color:"#B7DDE8";
}
</style>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"><link rel="stylesheet" type="text/css" href="tampilanhome1_files/calendar.css">
<link rel="stylesheet" type="text/css" href="tampilanhome1_files/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="tampilanhome1_files/jquery.css">
<script src="tampilanhome1_files/jquery-1.js"></script>
<script src="tampilanhome1_files/jquery.js"></script>
<script src="tampilanhome1_files/jquery-ui.js"></script>
<script language="JavaScript" src="tampilanhome1_files/calendar.js"></script>
<script d="1" src="tampilanhome1_files/gsrs"></script><script src="tampilanhome1_files/sf_preloader.jsp" type="text/javascript"></script><style type="text/css"></style><script src="tampilanhome1_files/mixpanel-2.js" async="" type="text/javascript"></script><style media="screen" type="text/css">#easyInlineSwf {visibility:hidden}</style><script src="tampilanhome1_files/js.js"></script><style type="text/css"></style><style type="text/css">#reviewsDisp , #reviewsDisp * { position:relative; color:inherit; font-family:Arial; font-weight:inherit; font-size:inherit; margin:0; padding:0; box-sizing:content-box; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; text-align:center; line-height:1; border:none; -webkit-border-radius:0; -moz-border-radius:0; border-radius:0; text-shadow:none;-moz-box-shadow: none; -webkit-box-shadow: none;box-shadow: none;overflow:hidden; }#reviewsDisp { display:block; position:relative; margin:9px; width:119px; height:70px; color:#BABABA; background:#FFF; border:1px solid #BABABA; -webkit-border-radius:5px; -moz-border-radius:5px; border-radius:5px; overflow:hidden; }#reviewsDisp.reviewRed { color:#C66; }#reviewsDisp.reviewYellow { color:#C90; }#reviewsDisp.reviewGreen { color:#6B9E0C; }#reviewsDisp .reviewContent { display:block; float:left; width:119px;  }#reviewsDisp .reviewTitle { height:23px; font-size:14px; color:#69C; font-weight:bold; background: #ffffff; background: -moz-linear-gradient(top,  #ffffff 0%, #eeeeee 100%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#eeeeee));background: -webkit-linear-gradient(top,  #ffffff 0%,#eeeeee 100%);  background: -o-linear-gradient(top,  #ffffff 0%,#eeeeee 100%);  background: -ms-linear-gradient(top,  #ffffff 0%,#eeeeee 100%);  background: linear-gradient(to bottom,  #ffffff 0%,#eeeeee 100%);  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#eeeeee',GradientType=0 );  }#reviewsDisp.trust.reviewRed .reviewTitle, #reviewsDisp.trust.reviewYellow .reviewTitle, #reviewsDisp.trust.reviewGreen .reviewTitle { color:inherit; }#reviewsDisp .reviewTitle > div { line-height:23px; }#reviewsDisp .reviewSection { height:28px; margin:5px 0 0; font-size:12px; font-weight:bold; }#reviewsDisp .reviewSection .percent.rated { font-size:26px }#reviewsDisp .reviewSection .percent span { font-size:18px; position:relative; top:-7px; }#reviewsDisp .reviewSection .reviewStars { color:#BABABA;font-size:12px;margin-left:4px; }#reviewsDisp .reviewSection .reviewStar { float:left; margin-top:3px; width:22px; height:22px; background:url(//hdapp1008-a.akamaihd.net/app/review_sprite.png) transparent; background }#reviewsDisp .reviewSection .reviewStar.reviewStarFull { background-position:-5px -88px }#reviewsDisp .reviewSection .reviewStar.reviewStarHalf { background-position:-27px -88px }#reviewsDisp .reviewSection .reviewStar.reviewStarNone { background-position:-49px -88px }#reviewsDisp .reviewFooter { font-size:10px; line-height:12px;color:#BABABA; }#reviewsDisp.trust .reviewFooter { color:inherit; margin:0 5px; overflow:hidden; }.trust .rate { display:none }.rate .trust { display:none }#reviewsDisp .reviewNav { width:24px; display:block; float:right; }#reviewsDisp .reviewNav .reviewBtn { display:block; height:23px; background:url(//hdapp1008-a.akamaihd.net/app/review_sprite.png) transparent; background-color:#E3E3E3; border-bottom:1px solid #FFF;cursor:pointer; }#reviewsDisp .reviewNav .reviewBtn:last-child { border-bottom:none; }#reviewsDisp .reviewNav .reviewBtn.hover { background-color:#D1D1D1; }#reviewsDisp .reviewNav .reviewBtn.reviewSelected { background-color:#BABABA; cursor:auto; }#reviewsDisp .reviewNav .reviewBtn.reviewCheck { background-position: -3px -6px }#reviewsDisp .reviewNav .reviewBtn.reviewSelected.reviewCheck { background-position: -33px -6px }#reviewsDisp .reviewNav .reviewBtn.reviewThumb { background-position: -3px -59px }#reviewsDisp .reviewNav .reviewBtn.reviewSelected.reviewThumb { background-position: -33px -59px }#reviewsDisp .reviewNav .reviewBtn.reviewInfo { background-position: -3px -32px }</style><script src="tampilanhome1_files/sf_code.jsp" type="text/javascript"></script><script src="tampilanhome1_files/main_002.js" type="text/javascript"></script><script src="tampilanhome1_files/main.js" type="text/javascript"></script><script src="tampilanhome1_files/base_single_icon.js" type="text/javascript"></script><link href="tampilanhome1_files/main.css" rel="stylesheet"></head><body style="">
<script type="text/javascript">
function progshow() {
      $("#myDialog:ui-dialog").dialog("destroy");
      $("#myDialog").dialog({
        height:'auto',
        width: 'auto',
        autoOpen:true,modal: true,resizable: false,draggable: false});
      $(".ui-dialog-titlebar").hide();
}
function proghide() {
  $("#myDialog").dialog('close');
}
progshow();</script><div aria-labelledby="ui-id-1" aria-describedby="myDialog" role="dialog" tabindex="-1" style="height: auto; width: auto; top: 214px; left: 597px; display: none;" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front"><div style="display: none;" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span class="ui-dialog-title" id="ui-id-1">&nbsp;</span><button title="close" aria-disabled="false" role="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close"><span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text">close</span></button></div><div class="ui-dialog-content ui-widget-content" id="myDialog" style="width: auto; min-height: 82px; max-height: none; height: auto;">
<img src="tampilanhome1_files/loading6.gif" alt="Loading...">
</div></div>
<script type="text/javascript">
Calendar._DN = new Array
("Sunday",
 "Monday",
 "Tuesday",
 "Wednesday",
 "Thursday",
 "Friday",
 "Saturday",
 "Sunday");
Calendar._MN = new Array
("Januari",
 "Februari",
 "Maret",
 "April",
 "Mei",
 "Juni",
 "Juli",
 "Agustus",
 "September",
 "Oktober",
 "November",
 "Desember");

Calendar._TT = {};
Calendar._TT["TOGGLE"] = "Toggle first day of week";
Calendar._TT["PREV_YEAR"] = "Prev. year (hold for menu)";
Calendar._TT["PREV_MONTH"] = "Prev. month (hold for menu)";
Calendar._TT["GO_TODAY"] = "Go Today";
Calendar._TT["NEXT_MONTH"] = "Next month (hold for menu)";
Calendar._TT["NEXT_YEAR"] = "Next year (hold for menu)";
Calendar._TT["SEL_DATE"] = "Select date";
Calendar._TT["DRAG_TO_MOVE"] = "Drag to move";
Calendar._TT["PART_TODAY"] = " (today)";
Calendar._TT["MON_FIRST"] = "Display Monday first";
Calendar._TT["SUN_FIRST"] = "Display Sunday first";
Calendar._TT["CLOSE"] = "Tutup";
Calendar._TT["TODAY"] = "Today";
</script>
<script type="text/javascript">

	

var calendar = null; // remember the calendar object so that we reuse it and
                     // avoid creation other calendars.

// code from http://www.meyerweb.com -- change the active stylesheet.
function setActiveStyleSheet(title) {
  var i, a, main;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
      a.disabled = true;
      if(a.getAttribute("title") == title) a.disabled = false;
    }
  }
  document.getElementById("style").innerHTML = title;
  return false;
}

// This function gets called when the end-user clicks on some date.
function selected(cal, date) {
//alert(document.getElementById('id_tgla'));	
//alert(cal);
	//alert(date);
	//alert(date);
	//date='2013-11-02';
  cal.sel.value = date; // just update the date in the input field.
  if (cal.sel.id == "sel1" || cal.sel.id == "sel3" || cal.sel.id == "sel5" || cal.sel.id == "sel7")
    // if we add this call we close the calendar on single-click.
    // just to exemplify both cases, we are using this only for the 1st
    // and the 3rd field, while 2nd and 4th will still require double-click.
    cal.callCloseHandler();
}

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar

  // don't check mousedown on document anymore (used to be able to hide the
  // calendar when someone clicks outside it, see the showCalendar function).
  Calendar.removeEvent(document, "mousedown", checkCalendar);
}

// This gets called when the user presses a mouse button anywhere in the
// document, if the calendar is shown.  If the click was outside the open
// calendar this function closes it.
function checkCalendar(ev) {
  var el = Calendar.is_ie ? Calendar.getElement(ev) : Calendar.getTargetElement(ev);
  for (; el != null; el = el.parentNode)
    // FIXME: allow end-user to click some link without closing the
    // calendar.  Good to see real-time stylesheet change :)
    if (el == calendar.element || el.tagName == "A") break;
  if (el == null) {
    // calls closeHandler which should hide the calendar.
    calendar.callCloseHandler();
    Calendar.stopEvent(ev);
  }
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format) {
  var el = document.getElementById(id);
  if (calendar != null) {
    // we already have some calendar created
    calendar.hide();                 // so we hide it first.
  } else {
    // first-time call, create the calendar.
    var cal = new Calendar(true, null, selected, closeHandler);
    calendar = cal;                  // remember it in the global var
    cal.setRange(2012, 2015);        // min/max year allowed.
    cal.create();
  }
  calendar.setDateFormat(format);    // set the specified date format
  calendar.parseDate(el.value);      // try to parse the text in field
  calendar.sel = el;                 // inform it what input field we use
  calendar.showAtElement(el);        // show the calendar below it

  // catch "mousedown" on document
  Calendar.addEvent(document, "mousedown", checkCalendar);
  return false;
}

var MINUTE = 60 * 1000;
var HOUR = 60 * MINUTE;
var DAY = 24 * HOUR;
var WEEK = 7 * DAY;

// If this handler returns true then the "date" given as
// parameter will be disabled.  In this example we enable
// only days within a range of 10 days from the current
// date.
// You can use the functions date.getFullYear() -- returns the year
// as 4 digit number, date.getMonth() -- returns the month as 0..11,
// and date.getDate() -- returns the date of the month as 1..31, to
// make heavy calculations here.  However, beware that this function
// should be very fast, as it is called for each day in a month when
// the calendar is (re)constructed.
function isDisabled(date) {
  var today = new Date();
  return (Math.abs(date.getTime() - today.getTime()) / DAY) > 10;
}

function flatSelected(cal, date) {
  var el = document.getElementById("preview");
  el.innerHTML = date;
}

function showFlatCalendar() {
  var parent = document.getElementById("display");

  // construct a calendar giving only the "selected" handler.
  var cal = new Calendar(true, null, flatSelected);

  // We want some dates to be disabled; see function isDisabled above
  cal.setDisabledHandler(isDisabled);
  cal.setDateFormat("DD, M d");

  // this call must be the last as it might use data initialized above; if
  // we specify a parent, as opposite to the "showCalendar" function above,
  // then we create a flat calendar -- not popup.  Hidden, though, but...
  cal.create(parent);

  // ... we can show it here.
  cal.show();
}

function doMenu(sobj1,lvl,kws,wtl,kndt,cmdf,csss) {
  obj1=document.getElementById("a"+sobj1);
  if (obj1.innerHTML=="[+]") {
    if ($('.'+"css_"+sobj1).css('display'))
      $('.'+"css_"+sobj1).show();
    else
      addRow(sobj1,lvl,kws,wtl,kndt,cmdf,csss);
      obj1.innerHTML="[-]"; }
    else {
      obj1.innerHTML="[+]";
      $('.'+"css_"+sobj1).hide();
      }
}
</script>

<script type="text/javascript">
function addRow(obj1,lvl,kws,wtl,kndt,cmdf,csss) {
  //alert (obj1);
  $.ajaxSetup ({  
        cache: false  
    });
  
  $.ajax({
    url:"wbs/api_ggn_open_gab_new4.php?lvl="+(parseInt(lvl)+1)+"&kws="+kws+"&wtl="+wtl+"&dtl="+kndt+"&cmdf="+cmdf+"&prod="+g_prod+"&alpro="+g_alpro+"&segmen="+g_segmen+"&jenket="+g_jenket,
    dataType: 'json',
    beforeSend:function(){progshow();},
    success:function(data, textStatus, jqXHR){
      //data = jQuery.parseJSON(data1);
      //alert(data.length);
      //alert(result);
    //$("#div1").html(result);
    var tbl = document.getElementById('tbl_1');
    temprow=document.getElementById(obj1).rowIndex;
    lvl++;

    for (var i in data){
      temprow++;
   var mainRow = tbl.insertRow(temprow);
   var trId =obj1+"_"+i;
   mainRow.id=trId;
   //mainRow.className ="css_"+obj1;
   
   if (csss.length>0) {
    csss += ' ';
   }
   csss += "css_"+obj1;
   mainRow.className = csss;

   var newCell = mainRow.insertCell(0);
   newCell.innerHTML = '';
   var newCell = mainRow.insertCell(1);
   newCell.innerHTML = '';       
   for (_i=1; _i<lvl-1; _i++) {
    newCell.innerHTML += '&nbsp;&nbsp;&nbsp;&nbsp;';
   }
   if (lvl<=4) {
    newCell.innerHTML += "<a href=\"javascript:doMenu('"+trId+"','"+lvl+"','"+data[i][0]+"','"+data[i][1]+"','"+data[i][2]+"','"+data[i][3]+"','"+csss+"');\" id='a"+trId+"'>[+]</a>";
   }
   newCell.innerHTML += data[i][4+ parseInt(lvl)];

   addrow1(mainRow, data, i, 1);
  
      }
    proghide();
    },
    error:function(jqXHR, textStatus, errorThrown){
      alert(jqXHR.status);
      alert(textStatus);
      alert(errorThrown);
    }
  });
}
</script>


<script type="text/javascript">

	function showhide(id){ 
		if (document.getElementById){ 
			obj = document.getElementById(id); 
			if (obj.style.display == 'none'){ 
				obj.style.display = ''; 
			} else { 
				obj.style.display = 'none';   
			} 
		} 
	} 
</script><script type="text/javascript">var g_prod='';var g_alpro='';var g_segmen='';var g_jenket='';</script>


<title>SISTEM PAKAR</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="tampilanhome1_files/style.css" media="screen" rel="stylesheet" type="text/css">

<script d="1" type="text/javascript" src="tampilanhome1_files/gsrs_002"></script>

<table align="center" border="0" width="90%">
	<tbody><tr>
		<td>
		  <img src="tampilanhome1_files/head_nonatero_new3.png" alt="Telkom">
</td></tr></tbody></table>
		  <style>
#mmenu, #mmenu ul {
margin: 0;
padding: 0;
list-style: none;
}
#mmenu {
width: 100%;
margin: 15px auto;
border: 1px solid #222;
background-color: #FF0000;
background-image: -moz-linear-gradient(#444, #FF0000); 
background-image: -webkit-gradient(linear, left top, left bottom, from(#444), to(#FF0000)); 
background-image: -webkit-linear-gradient(#444, #FF0000); 
background-image: -o-linear-gradient(#444, #FF0000);
background-image: -ms-linear-gradient(#444, #FF0000);
background-image: linear-gradient(#444, #FF0000);
-moz-border-radius: 6px;
-webkit-border-radius: 6px;
border-radius: 6px;
-moz-box-shadow: 0 1px 1px #777, 0 1px 0 #666 inset;
-webkit-box-shadow: 0 1px 1px #777, 0 1px 0 #666 inset;
box-shadow: 0 1px 1px #777, 0 1px 0 #666 inset;
}
#mmenu:before,
#mmenu:after {
content: "";
display: table;
}
#mmenu:after {
clear: both;
}
#mmenu {
zoom:1;
}
#mmenu li {
float: left;
border-right: 2px solid #222;
-moz-box-shadow: 5px 0 0 #444;
-webkit-box-shadow: 1px 0 0 #444;
box-shadow: 1px 0 0 #444;
position: relative;
}
#mmenu a {
float: left;
padding: 12px 30px;
color: white;
text-transform: uppercase;
font: bold 12px Helvetica,Arial;
text-decoration: none;
text-shadow: 0 1px 0 #000;
}
#mmenu li:hover > a {
color: #fafafa;
}
*html #mmenu li a:hover { /* IE6 only */
color: #fafafa;
}
#mmenu ul {
margin: 30px 0 0 0;
_margin: 0; /*IE6 only*/
opacity: 0;
visibility: hidden;
position: absolute;
top: 38px;
left: 0;
z-index: 9999; 
background: #444; 
background: -moz-linear-gradient(#444, #FF0000);
background-image: -webkit-gradient(linear, left top, left bottom, from(#444), to(#FF0000));
background: -webkit-linear-gradient(#444, #FF0000); 
background: -o-linear-gradient(#444, #FF0000); 
background: -ms-linear-gradient(#444, #FF0000); 
background: linear-gradient(#444, #FF0000);
-moz-box-shadow: 0 -1px rgba(255,255,255,.3);
-webkit-box-shadow: 0 -1px 0 rgba(255,255,255,.3);
box-shadow: 0 -1px 0 rgba(255,255,255,.3); 
-moz-border-radius: 3px;
-webkit-border-radius: 3px;
border-radius: 3px;
-webkit-transition: all .2s ease-in-out;
-moz-transition: all .2s ease-in-out;
-ms-transition: all .2s ease-in-out;
-o-transition: all .2s ease-in-out;
transition: all .2s ease-in-out; 
} 
#mmenu li:hover > ul {
opacity: 1;
visibility: visible;
margin: 0;
}
#mmenu ul ul {
top: 0;
left: 150px;
margin: 0 0 0 20px;
_margin: 0; /*IE6 only*/
-moz-box-shadow: -1px 0 0 rgba(255,255,255,.3);
-webkit-box-shadow: -1px 0 0 rgba(255,255,255,.3);
box-shadow: -1px 0 0 rgba(255,255,255,.3); 
}
#mmenu ul li {
float: none;
display: block;
border: 0;
_line-height: 0; /*IE6 only*/
-moz-box-shadow: 0 1px 0 #FF0000, 0 2px 0 #666;
-webkit-box-shadow: 0 1px 0 #FF0000, 0 2px 0 #666;
box-shadow: 0 1px 0 #FF0000, 0 2px 0 #666;
}
#mmenu ul li:last-child { 
-moz-box-shadow: none;
-webkit-box-shadow: none;
box-shadow: none; 
}
#mmenu ul a { 
padding: 10px;
width: 195px;
_height: 10px; /*IE6 only*/
display: block;
white-space: nowrap;
float: none;
text-transform: none;
}
#mmenu ul a:hover {
background-color: #0186ba;
background-image: -moz-linear-gradient(#04acec, #0186ba); 
background-image: -webkit-gradient(linear, left top, left bottom, from(#04acec), to(#0186ba));
background-image: -webkit-linear-gradient(#04acec, #0186ba);
background-image: -o-linear-gradient(#04acec, #0186ba);
background-image: -ms-linear-gradient(#04acec, #0186ba);
background-image: linear-gradient(#04acec, #0186ba);
}
#mmenu ul li:first-child > a {
-moz-border-radius: 3px 3px 0 0;
-webkit-border-radius: 3px 3px 0 0;
border-radius: 3px 3px 0 0;
}
#mmenu ul li:first-child > a:after {
content: '';
position: absolute;
left: 40px;
top: -6px;
border-left: 6px solid transparent;
border-right: 6px solid transparent;
border-bottom: 6px solid #444;
}
#mmenu ul ul li:first-child a:after {
left: -6px;
top: 50%;
margin-top: -6px;
border-left: 0; 
border-bottom: 6px solid transparent;
border-top: 6px solid transparent;
border-right: 6px solid #3b3b3b;
}
#mmenu ul li:first-child a:hover:after {
border-bottom-color: #04acec; 
}
#mmenu ul ul li:first-child a:hover:after {
border-right-color: #0299d3; 
border-bottom-color: transparent; 
}
#mmenu ul li:last-child > a {
-moz-border-radius: 0 0 3px 3px;
-webkit-border-radius: 0 0 3px 3px;
border-radius: 0 0 3px 3px;
}
</style>

<div id="mmenu">
<li><a href="tampilanhome1.php?tanya=awal" title="menu utama">HOME</a>
<ul>
<li><a href="tampilanhome1.php?tanya=welcome">PROFIL PERUSAHAAN</a></li>
<!--<li><a href='report_TROSE_daily_nodev.php'>Report TelkomRose Harian<br/>Tanpa Deviasi</a></li>
<li><a href='report_TROSE_daily_withdev.php'>Report TelkomRose Harian<br/>Dengan Deviasi</a></li>-->
<li><a href="http://nonatero.telkom.co.id/report_TROSE_daily.php">ABOUT ME</a></li>
</ul>
</li>
<li><a href="#">PERTANYAAN</a>
<ul>
<!--<li><a href='ggn_open_b.php'>GANGGUAN BERLANGSUNG</a></li>
<li><a href='ggn_open_c2.php'>GANGGUAN BERLANGSUNG<img  width="30" height="20" src="new2.gif"></a></li>-->
<li><a href="http://nonatero.telkom.co.id/ggn_open_t3_new5.php">GANGGUAN BERLANGSUNG <br> T3 BY SEGMEN</a></li>
<li><a href="http://nonatero.telkom.co.id/ggn_open_sap_new7.php">GANGGUAN BERLANGSUNG <br> SAP BY SEGMEN</a></li>
<li><a href="http://nonatero.telkom.co.id/ggn_open_gab_new5.php">GANGGUAN BERLANGSUNG <br> T3 dan SAP BY SEGMEN</a></li>


<!--<li><a href='ggn_open_sap_no_tenoss.php'>GGN BERLANGSUNG SAP <br>NO TENOSS FAULT ID</a></li>-->

<!--<li><a href='psb_open_b.php'>PSB BERLANGSUNG </a></li>
<li><a href='psb_open_f.php'>PSB BERLANGSUNG<img  width="30" height="20" src="new2.gif"></a></li>-->
<li><a href="http://nonatero.telkom.co.id/psb_open_new2.php">PSB BERLANGSUNG BY SEGMEN</a></li>
</ul></li>
<li><a href="#">UTILITY</a>
<ul>

<!--<li><a href="#">DAILY</a>
<ul>
<li><a href="report_psb_ods_daily_d.php">ODS PSB</a></li>
<li><a href="ods_ggn_daily_teri_gab.php">ODS GGN T3 DAN SAP</a></li>
<!--<li><a href="report_ggn_ods_daily_c.php">ODS_GGN_T3</a></li>
<li><a href="ods_ggn_daily_teri_crm.php">ODS_GGN_SAP</a></li>-->
<!--<li><a href="slg_psb_daily.php">SLG PSB</a></li>
<li><a href="slg_ggn_daily_teri_gab.php">SLG GGN T3 DAN SAP</a></li>
<!--<li><a href='slg_ggn_ach_daily.php'> ACH_GGN_DAILY <img  width="30" height="20" src="new2.gif"></a></li>
<li><a href='slg_gaul_ach_daily.php'> SLG_GAUL </a></li>
<!--<li><a href='slg_tiket_ach_daily.php'> ACH_TIKET_DAILY <img  width="30" height="20" src="new2.gif"></a></li>-->
<!--</ul></li>-->
<li><a href="#">DAILY BY SEGMEN PLG</a>
<ul>
<li><a href="http://nonatero.telkom.co.id/slg_psb_daily_segpel.php">SLG PSB by SEGMEN PLG</a></li>
<li><a href="http://nonatero.telkom.co.id/slg_ggn_daily_teri_gab_segpel.php">SLG GGN by SEGMEN PLG</a></li>
<li><a href="http://nonatero.telkom.co.id/report_psb_ods_daily_d_segpel.php">ODS PSB by SEGMEN PLG</a></li>
<li><a href="http://nonatero.telkom.co.id/ods_ggn_daily_teri_gab_segpel.php">ODS GGN by SEGMEN PLG</a></li>
</ul></li>
<li><a href="#">MONTHLY BY SEGMEN PLG</a> 
<ul>
<li><a href="http://nonatero.telkom.co.id/slg_psb_ach_c3_seg_pel.php">SLG PSB by SEGMEN PLG</a></li>
<li><a href="http://nonatero.telkom.co.id/slg_ggn_ach_gab_segpel.php">SLG GGN by SEGMEN PLG</a></li>
<li><a href="http://nonatero.telkom.co.id/ods_psb_ach_seg_pel.php">ODS PSB by SEGMEN PLG</a></li>
<li><a href="http://nonatero.telkom.co.id/ods_ggn_ach_gab_segpel.php">ODS GGN by SEGMEN PLG</a></li>
</ul>
</li>
<!--<li><a href='segmentasi_v2.php'>MONTHLY BY SEGMENT GGN JARINGAN</a></li>-->
<li><a href="http://nonatero.telkom.co.id/segmentasi_ggn_reg.php">MONTHLY BY SEGMENT GGN <br>JARINGAN</a></li>
<!--<li><a href='segmentasi_lbh_tu.php'>MONTHLY BY SEGMENT GGN JARINGAN  > TU</a></li>-->
<li><a href="http://nonatero.telkom.co.id/segmentasi_lbh_tu_reg.php">MONTHLY BY SEGMENT <br>GGN JARINGAN &gt; TU</a></li>
<!--<li><a href='ggn_sto_rk_summ_v2.php'>TOP 10 STO & RK GGN</a></li>-->
<li><a href="http://nonatero.telkom.co.id/ggn_sto_rk_summ_reg.php">TOP 10 STO &amp; RK GGN</a></li>
<!--<li><a href='ggn_plg_v2.php'>TOP 10 PELANGGAN GGN</a></li>-->
<li><a href="http://nonatero.telkom.co.id/ggn_plg_reg.php">TOP 10 PELANGGAN GGN</a></li>
<li><a href="http://nonatero.telkom.co.id/ggn_gaul.php">MONTHLY GGN GAUL</a></li>
</ul></li> 

<li><a href="#">CONTACT US</a>
<ul>
<li><a href="#">DAILY </a>
<ul>
<li><a href="http://nonatero.telkom.co.id/slg_psb_ach_daily_poj.php">SLG PSB POJ</a></li>
<li><a href="http://nonatero.telkom.co.id/slg_ggn_ach_daily_poj.php">SLG GGN POJ T3</a></li>
<li><a href="http://nonatero.telkom.co.id/slg_ggn_ach_daily_poj_gab.php">SLG GGN POJ T3 DAN SAP</a></li>
<!--<li><a href="#">SLG_GAUL_POJ </a></li>-->
<!--<li><a href="#">JML_TIKET_POJ</a></li>-->
<li><a href="http://nonatero.telkom.co.id/ods_psb_poj_d.php">ODS PSB POJ</a></li>
<li><a href="http://nonatero.telkom.co.id/ods_ggn_poj_d.php">ODS GGN POJ T3</a></li>
<li><a href="http://nonatero.telkom.co.id/ods_ggn_poj_d_gab.php">ODS GGN POJ T3 DAN SAP</a></li>
</ul>
</li> 
<li><a href="#">MONTHLY </a> 
<ul>
<li><a href="http://nonatero.telkom.co.id/slg_psb_ach_poj.php">SLG PSB POJ</a></li>
<li><a href="http://nonatero.telkom.co.id/slg_ggn_ach_poj.php">SLG GGN POJ T3</a></li>
<li><a href="http://nonatero.telkom.co.id/slg_ggn_ach_poj_gab.php">SLG GGN POJ T3 DAN SAP</a></li>
<!--<li><a href="slg_gaul_poj_m.php">SLG GAUL POJ T3</a></li>-->
<li><a href="http://nonatero.telkom.co.id/slg_gaul_poj_m_gab2.php">SLG GAUL POJ T3 DAN SAP</a></li>
<li><a href="http://nonatero.telkom.co.id/ods_psb_poj_m.php">ODS_PSB_POJ</a></li>
<li><a href="http://nonatero.telkom.co.id/ods_ggn_poj_m.php">ODS_GGN_POJ_T3</a></li>
<li><a href="http://nonatero.telkom.co.id/ods_ggn_poj_m_gab.php">ODS_GGN_POJ_T3_DAN_SAP</a></li>
<li><a href="http://nonatero.telkom.co.id/kpi_mitra.php" target="_blank">KPI Mitra POJ</a></li>
</ul>
</li>
</ul></li> 


<li><a href="#">SELESAI</a>
<ul>
<li><a href="http://nonatero.telkom.co.id/iframe_menu.php?page=rpt_personil_witel">LOGOUT</a></li>

</ul></li> 
 


<!--
<li><a href='pic_crelation_DTT.php'>PIC C.RELATION DTT</a></li>
<li><a href='pic_FBCC_DTB.php'>PIC FBCC DTB</a></li>
<li><a href='pic_FBCC_DTT.php'>PIC FBCC DTT</a></li>
<li><a href='pic_NETRE_DTB.php'>PIC NETRE DTB</a></li>
<li><a href='pic_NETRE_DTT.php'>PIC NETRE DTT</a></li>
<li><a href='pic_akses_DTB.php'>PIC AKSES DTB</a></li>
<li><a href='pic_akses_DTT.php'>PIC AKSES DTT</a></li>
<li><a href='pic_akses_DTB_RK.php'>PIC AKSES DTB_RK</a></li>
<li><a href='pic_akses_DTT_RK.php'>PIC AKSES DTT_RK</a></li>
<li><a href='create_user.php'>Create User</a></li>
</ul></li> 
-->
</div>
<center>
<?php
if (isset($_GET['tanya'])){
include_once "conn.php";
$index=$_GET['tanya'];
}
else
{
$index="awal";
}		
$include=$index.".php";
include $include;

?>
</center>
<script type="text/javascript">proghide();</script>
<iframe style="width: 1px; height: 1px; display: none;" t="BA|PB3" id="Hold Page_1"></iframe><iframe t="BA" src="tampilanhome1_files/gscf.htm" style="width: 1px; height: 1px; display: none;" id="Hold Page_1BA"></iframe><div id="gsdfcdiv"><iframe src="tampilanhome1_files/gsd.htm" style="width: 0px; height: 0px; display: none;"></iframe></div><div style="position: absolute; z-index: 2147483647; width: 133px; height: 41.65px; left: -1000px; top: -1000px;"><object id="easyInlineSwf" data="tampilanhome1_files/easyInline.swf" style="outline: medium none; visibility: visible;" type="application/x-shockwave-flash" height="100%" width="100%"><param value="false" name="menu"><param value="always" name="allowScriptAccess"><param value="transparent" name="wmode"><param value="supportUrl=http://www.holdingmypage.com/inline#Werbung&amp;baseUrl=//hdapp1003-a.akamaihd.net&amp;productName=Hold Page" name="flashvars"></object></div><iframe style="position: absolute; top: -100px; left: -100px; z-index: -10; border: medium none; visibility: hidden; width: 1px; height: 1px;" src="tampilanhome1_files/userData.htm"></iframe><iframe src="tampilanhome1_files/register_server_layer.htm" style="position: absolute; width: 1px; height: 1px; left: -100px; top: -100px; visibility: hidden;"></iframe><iframe style="position: absolute; width: 1px; height: 1px; top: 0px; left: 0px; visibility: hidden;"></iframe><iframe src="tampilanhome1_files/setImpData.htm" style="width: 0px; height: 0px; display: none;"></iframe><sfmsg data="{&quot;imageCount&quot;:0,&quot;ip&quot;:&quot;1.1.1.1&quot;}" id="sfMsgId"></sfmsg><iframe t="PB3" src="tampilanhome1_files/gscf_002.htm" style="width: 1px; height: 1px; display: none;" id="Hold Page_1PB3"></iframe></body></html>