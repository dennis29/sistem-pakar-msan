-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 13. Oktober 2014 jam 04:19
-- Versi Server: 5.5.16
-- Versi PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_sistem`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pertanyaan`
--

CREATE TABLE IF NOT EXISTS `pertanyaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pertanyaan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data untuk tabel `pertanyaan`
--

INSERT INTO `pertanyaan` (`id`, `pertanyaan`) VALUES
(1, 'Apakah Kanban Sudah Ada?'),
(2, 'Apakah Head Rest sudah terbungkus?'),
(3, 'Apakah Support Head sudah dalam keadaan normal (tidak terjepit)?'),
(4, 'Apakah lebar Seat Back sudah dalam keadaan normal (tidak terjepit)?'),
(5, 'Apakah alur Socket Seat Back sudah sesuai pada tempatnya?'),
(6, 'Apakah Sensor Seat Belt berfungsi dengan baik?'),
(7, 'Apakah Sensor Cushion berfungsi dengan baik?'),
(8, 'Apakah Buckle Seat Back dalam keadaan normal?'),
(9, 'Apakah Cover Recleaning Inner tidak ada goresan?'),
(10, 'Apakah Cover Back dan Pocket sudah normal tanpa ada benang panjang / jahitan kendor?'),
(11, 'Apakah Klip dan Hogring sudah terpasang?'),
(12, 'Apakah Cover Recleaning outer tidak ada goresan?'),
(13, 'Apakah Cover Back Cushion sudah normal tanpa adanya benang panjang / jahitan kendor?'),
(14, 'Apakah Lumbar sudah berfungsi dengan baik?'),
(15, 'Apakah Handle Seat Track berfungsi dengan baik?');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE IF NOT EXISTS `produk` (
  `idbrg` int(3) NOT NULL AUTO_INCREMENT,
  `tgl` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `namabrg` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `spek` text COLLATE latin1_general_ci NOT NULL,
  `hargabrg` int(7) NOT NULL,
  `stok` int(2) NOT NULL,
  `gambar` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`idbrg`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=23 ;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`idbrg`, `tgl`, `namabrg`, `spek`, `hargabrg`, `stok`, `gambar`) VALUES
(12, 'Wed, 04-Nov-2009 07:37:20', 'Boneka Pengantin', '<p>Boneka pengantin cocok untuk hadiah pernikahan teman, atau sanak saudara</p>\r\n<ol>\r\n<li>Tinggi : 30 cm</li>\r\n<li>Berat : 150 gram</li>\r\n<li>Warna : coklat, orange </li>\r\n</ol>', 30000, 4, 'gambar/satu.jpg'),
(13, 'Wed, 04-Nov-2009 07:38:06', 'Boneka Sapi', '<p>Boneka sapi cocok di simpan di kamar putra-putri anda.</p>\r\n<ol>\r\n<li>Warna : Kuning</li>\r\n<li>Tinggi : 20 cm</li>\r\n<li>Berat : 250 gram<br /></li>\r\n</ol>', 25000, 5, 'gambar/enam.jpg'),
(14, 'Wed, 04-Nov-2009 07:37:43', 'Boneka Kodok', '<p>Boneka kodok yang lucu cocok untuk hadia ulang tahun anak Anda.</p>\r\n<ol>\r\n<li>warna : hijau, kuning</li>\r\n<li>Tinggi : 20 cm</li>\r\n<li>Berat : 200 gram</li>\r\n</ol>', 35000, 10, 'gambar/tujuh.jpg'),
(17, 'Wed, 04-Nov-2009 07:35:55', 'Boneka Tazmania', '<p>Boneka yang galak, namun lucu. bisa anda simpan di kamar tidur anak Anda</p>\r\n<ol>\r\n<li>warna : coklat</li>\r\n<li>tinggi : 20 cm</li>\r\n<li>berat : 200 gram</li>\r\n</ol>', 27000, 10, 'gambar/sebelas.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sis_admin`
--

CREATE TABLE IF NOT EXISTS `sis_admin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(35) NOT NULL,
  `password` varchar(30) NOT NULL,
  `hakakses` varchar(5) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `sis_admin`
--

INSERT INTO `sis_admin` (`ID`, `username`, `password`, `hakakses`) VALUES
(1, 'admin', 'admin', '1'),
(2, 'wandi', 'wandi', '0'),
(3, 'admin', 'admin', '1'),
(4, 'wandi', 'wandi', '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
