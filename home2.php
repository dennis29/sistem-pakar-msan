<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION['MM_Username'] = NULL;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
	
  $logoutGoTo = "index.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="jquery-1.7.1.min.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sistem Pakar</title>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="styles/style_admin.css" rel="stylesheet" type="text/css">
<style type="text/css">
body,td,th,tr {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #B7DDE8;
	background-color:"#B7DDE8";
}
</style></head>
<body><center>
<div style="margin:5px; padding:5px;">
<table width="900" border="10" align="center" cellpadding="0" cellspacing="0" class="table-list" cellspacing="1" bgcolor="#999999">
  <tr>
    <td><img src="images/header.jpg" /></td>
  </tr>
  <tr>
    <td bgcolor="#B7DDE8"><marquee> Saat ini anda sedang menggunakan aplikasi sistem pakar </marquee></td>
  </tr>
  <tr>
    <td bgcolor="#A6D4E1"></td>
  </tr>
  <tr>
    <td bgcolor="#CCCCCC">
	<ul id="MenuBar1" class="MenuBarHorizontal">
      <li><a href="home.php?tanya=awal" title="menu utama">home </a>
	  <ul>
	  <li><a href="home.php?tanya=welcome">Profil Perusahaan</a></li>
	  <li><a href="home.php?tanya=about">About Me</a></li>
          
	</ul>
	  </li>
            <li><a href="#" title="about" class="MenuBarItemSubmenu">Pertanyaan</a>
        <ul>
          <li><a href="home.php?tanya=tanya&tbl=pertanyaan1&no=1">Pertanyaan 1</a></li>
		   <li><a href="home.php?tanya=tanya&tbl=pertanyaan2&no=1">Pertanyaan 2</a></li>
		    <li><a href="home.php?tanya=tanya&tbl=pertanyaan3&no=1">Pertanyaan 3</a></li>
		  	 <li><a href="home.php?tanya=tanya&tbl=pertanyaan4&no=1">Pertanyaan 4</a></li>
 </ul>
      </li>
      <li><a href="#" title="about" class="MenuBarItemSubmenu" >Utility</a>
        <ul>
			<?php 
			if($_SESSION['MM_level']=="1"){?>
          <li><a href="home.php?index=1">Add Admin</a></li>
          <li><a href="home.php?index=6">Add User</a></li> 
		  <li><a href="home.php?index=17">Ubah / Hapus Admin</a></li>
		  <li><a href="home.php?index=18">Ubah / Hapus User</a></li>
		  <?php }?>
          <li><a href="home.php?index=4">Ganti Password</a></li>
		  <?php
		  if($_SESSION['MM_level']=="1"){?>
          <li><a href="home.php?tanya=tabel_backup2">Backup Data Ke Excell</a>
		  </li>
		  <?php ; }?>
        </ul>
      </li>
	  <li><a href="#" title="model" class="MenuBarItemSubmenu">Contact Us </a>
        <ul>
          <li><a href="#">HelpDesk</a>
		   <ul class="MenuBarVertical">
          <li><a href="home.php?index=12&jnsmobil=innova&jnsseat=fr&no=1">call 02134353637</a></li>
        </ul></li>
          <li><a href="#">Central</a>
		  <ul class="MenuBarVertical">
		  <li><a href="home.php?index=12&jnsmobil=fortuner&jnsseat=fr&no=1">call 0218842300</a></li>
        </ul></li>
        </ul>
      </li>
      <li><a href="#">selesai</a>
	  <ul>
	  <li><a href="<?php echo $logoutAction; ?>">Logout</a></li> 
	</ul></li>
    </ul></td>
  </tr>
  <script type="text/javascript">
   $(document).ready(function() {
            $("input").focus(function(e) {
         $(e.target).css("background-color","silver");
      });
      $("input").blur(function(e) {
         $(e.target).css("background-color","white");
      });
	  
   });
</script>
        <tr>
        <td align="center" height="400">
<?php
if (isset($_GET['tanya'])){
include_once "conn.php";
$index=$_GET['tanya'];
}
else
{
$index="awal";
}		
$include=$index.".php";
include $include;

?>
		</td>
      </tr>
  <tr>
    <td align="center" bgcolor="#B7DDE8">@copyright by Yasir Suhari ,2014</td>
  </tr>
</table>
<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"SpryAssets/SpryMenuBarDownHover.gif", imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
//-->

</script>
</center>
</body>
</html>