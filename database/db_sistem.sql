-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 09. Nopember 2014 jam 12:21
-- Versi Server: 5.5.16
-- Versi PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_sistem`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenispakar`
--

CREATE TABLE IF NOT EXISTS `jenispakar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) NOT NULL,
  `nilai` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `jenispakar`
--

INSERT INTO `jenispakar` (`id`, `jenis`, `nilai`) VALUES
(1, 'pertanyaan_jok_depan_seat_front', 'Jok Depan Seat Front'),
(2, 'pertanyaan_jok_tengah_seat_rear_satu', 'Jok Tengah Seat Rear Satu'),
(3, 'pertanyaan_jok_belakang_seat_rear_2', 'Jok Belakang Seat Rear Dua'),
(4, 'pertanyaan_sewing_jok_belakang_seat_rear_2', 'sewing');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pertanyaan_jok_belakang_seat_rear_2`
--

CREATE TABLE IF NOT EXISTS `pertanyaan_jok_belakang_seat_rear_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pertanyaan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data untuk tabel `pertanyaan_jok_belakang_seat_rear_2`
--

INSERT INTO `pertanyaan_jok_belakang_seat_rear_2` (`id`, `pertanyaan`) VALUES
(1, 'Apakah Kanban Sudah Ada?'),
(2, 'Apakah Head Rest sudah terbungkus?'),
(3, 'Apakah support head rest tidak terjepit?'),
(4, 'Apakah seat belt sudah terpasang dengan benar?'),
(5, 'Apakah cover back dan cashion sudah terpasang dengan benar (tidak ada benang panjang dan jahitan ken'),
(6, 'Apakah bracket leg sudah terpasang dan berfungsi dengan benar?'),
(7, 'Apakah cover shield sudah terpasang dengan benar dan tidak ada goresan?'),
(8, 'Apakah cover recleaning inner sudah terpasang dengan benar dan tidak ada goresan?'),
(9, 'Apakah cover recleaning outer dan handle sudah terpasang dengan benar dan tidak ada goresan?'),
(10, 'Apakah label information sudah terpasang dengan benar?'),
(11, 'Apakah under cover/karpet cashion sudah terpasang dengan benar (tidak ada benang panjang dan jahitan'),
(12, 'Apakah cover lock assy sudah terpasang dan berfungsi dengan benar?'),
(13, 'Apakah cover hinge sudah terpasang dengan benar dan tidak ada goresan?'),
(14, 'Apakah handle release sudah terpasang dengan benar?'),
(15, 'Apakah band assy sudah terpasang dengan benar?');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pertanyaan_jok_depan_seat_front`
--

CREATE TABLE IF NOT EXISTS `pertanyaan_jok_depan_seat_front` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pertanyaan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data untuk tabel `pertanyaan_jok_depan_seat_front`
--

INSERT INTO `pertanyaan_jok_depan_seat_front` (`id`, `pertanyaan`) VALUES
(1, 'Apakah Kanban Sudah Ada?'),
(2, 'Apakah Head Rest sudah terbungkus?'),
(3, 'Apakah Support Head sudah dalam keadaan normal (tidak terjepit)?'),
(4, 'Apakah lebar Seat Back sudah dalam keadaan normal (tidak terjepit)?'),
(5, 'Apakah alur Socket Seat Back sudah sesuai pada tempatnya?'),
(6, 'Apakah Sensor Seat Belt berfungsi dengan baik?'),
(7, 'Apakah Sensor Cushion berfungsi dengan baik?'),
(8, 'Apakah Buckle Seat Back dalam keadaan normal?'),
(9, 'Apakah Cover Recleaning Inner tidak ada goresan?'),
(10, 'Apakah Cover Back dan Pocket sudah normal tanpa ada benang panjang / jahitan kendor?'),
(11, 'Apakah Klip dan Hogring sudah terpasang?'),
(12, 'Apakah Cover Recleaning outer tidak ada goresan?'),
(13, 'Apakah Cover Back Cushion sudah normal tanpa adanya benang panjang / jahitan kendor?'),
(14, 'Apakah Lumbar sudah berfungsi dengan baik?'),
(15, 'Apakah Handle Seat Track berfungsi dengan baik?');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pertanyaan_jok_tengah_seat_rear_satu`
--

CREATE TABLE IF NOT EXISTS `pertanyaan_jok_tengah_seat_rear_satu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pertanyaan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data untuk tabel `pertanyaan_jok_tengah_seat_rear_satu`
--

INSERT INTO `pertanyaan_jok_tengah_seat_rear_satu` (`id`, `pertanyaan`) VALUES
(1, 'Apakah Kanban Sudah Ada dan Sesuai?'),
(2, 'Apakah back bagian belakang dan hanger sudah terpasang dengan baik?'),
(3, 'Apakah label informasi sudah terpasang dengan baik?'),
(4, 'Apakah cover recleaning inner sudah terpasang dengan benar dan tidak ada gores?'),
(5, 'Apakah cover recleaning outer sudah terpasang dengan benar dan tidak ada gores?'),
(6, 'Apakah handle realease sudah terpasang dan tidak ada kotoran?'),
(7, 'Apakah handle lock sudah berfungsi?'),
(8, 'Apakah cover lock oute sudah terpasang dengan benar dan tidak ada goresan?'),
(9, 'Apakah nut hinge outer sudah terpasang dengan benar?'),
(10, 'Apakah cover lock inner sudah terpasang dengan benar dan tidak ada gores?'),
(11, 'Apakah nut hinge inner sudah terpasang dengan benar?'),
(12, 'Apakah under cover sudah terpasang dengan benar dan tidak ada gores?'),
(13, 'Apakah headrest sudah berfungsi dengan benar?'),
(14, 'Apakah support headrest tidak terjepit?'),
(15, 'Apakah seat belt sudah terpasang dengan benar?'),
(16, 'Apakah cover back dan cashion sudah terpasang dengan benar (tidak ada benang panjang/jahitan kendor)'),
(17, 'Apakah handle seat track sudah berfungsi dengan benar?');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pertanyaan_sewing_jok_belakang_seat_rear_2`
--

CREATE TABLE IF NOT EXISTS `pertanyaan_sewing_jok_belakang_seat_rear_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pertanyaan` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `pertanyaan_sewing_jok_belakang_seat_rear_2`
--

INSERT INTO `pertanyaan_sewing_jok_belakang_seat_rear_2` (`id`, `pertanyaan`) VALUES
(1, 'Cek kanban Apakah sudah sesuai dengan model?');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sis_admin`
--

CREATE TABLE IF NOT EXISTS `sis_admin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(35) NOT NULL,
  `password` varchar(50) NOT NULL,
  `hakakses` varchar(5) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Dumping data untuk tabel `sis_admin`
--

INSERT INTO `sis_admin` (`ID`, `username`, `password`, `hakakses`) VALUES
(1, 'admin', 'simorangkir222', '1'),
(42, 'fg', '1', '1'),
(9, 'asi', 'satu', '1'),
(10, 'kkm', '9', '1'),
(70, 'neneng', 'dua', '1'),
(60, 'denis', 'simorangkir', '0'),
(58, 'wandi', 'satu', '1'),
(64, 'denias', 'ggkgkgkggkgkgkggkgkgkgkgkgkgk', '0'),
(62, 'ansi', '3', '0'),
(69, 'mimin', 'satu', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `solusi_pertanyaan_jok_belakang_seat_rear_2`
--

CREATE TABLE IF NOT EXISTS `solusi_pertanyaan_jok_belakang_seat_rear_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `solusi` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data untuk tabel `solusi_pertanyaan_jok_belakang_seat_rear_2`
--

INSERT INTO `solusi_pertanyaan_jok_belakang_seat_rear_2` (`id`, `solusi`) VALUES
(1, 'Check kembali kanban, harus sesuai dengan model seat.'),
(2, 'Check kembali head rest, harus rapih, tidak seret dan loss.'),
(3, 'Check kembali support head rest, harus terpasang dengan benar.'),
(4, 'Check kembali seat belt, harus terpasang dengan benar dan berfungsi dengan baik.'),
(5, 'Check kembali cover back dan cashion, harus sesuai spect dan standar keriput.'),
(6, 'Check kembali breacket leg, harus terpasang dengan kuat dan benar (bisa berputar).'),
(7, 'Check kembali cover shield, harus terpasang dengan benar, tidak seret dan kendor.'),
(8, 'Check kembali cover recleaning inner, harus terpasang dengan benar, tidak seret dan kendor.'),
(9, 'Check kembali cover recleaning outer dan handle recleaning, harus terpasang dengan benar dan tidak l'),
(10, 'Check kembali label information, harus terpasang dengan benar, tidak burry dan tidak lepas. '),
(11, 'Check kembali under cover/karpet cashion, harus rapih dan sesuai sample yang terpasang.'),
(12, 'Check kembali cover lock assy, harus terpasang dengan benar, tidak baret dan kendor.'),
(13, 'Check kembali cover hinge, harus terpasang dengan benar, tidak baret dan kendor.'),
(14, 'Check kembali handle release, harus terpasang dengan benar dan tidak lepas.'),
(15, 'Check kembali band assy, harus terpasang dengan benar, tidak baret dan ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `solusi_pertanyaan_jok_depan_seat_front`
--

CREATE TABLE IF NOT EXISTS `solusi_pertanyaan_jok_depan_seat_front` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `solusi` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data untuk tabel `solusi_pertanyaan_jok_depan_seat_front`
--

INSERT INTO `solusi_pertanyaan_jok_depan_seat_front` (`id`, `solusi`) VALUES
(1, 'Check kembali kanban, harus sesuai dengan model seat.'),
(2, 'Check kembali head rest, harus rapih, tidak seret dan loss.'),
(3, 'Check kembali support head rest, harus terpasang dengan benar.'),
(4, 'Check kembali lebar seat back, harus masuk dalam gauge.'),
(5, 'Check kembali alur socket seat belt, harus terpasang dengan benar, posisi soclet sesuai standar.'),
(6, 'Check kembali sensor seat belt, lampu indikator harus berfungsi.'),
(7, 'Check kembali sensor cashion, lampu indikator harus menyala.'),
(8, 'Check kembali buckel seat belt, harus terpasang dengan benar.'),
(9, 'Check cover recleaning inner, harus terpasang dengan benar dan tidak lepas.'),
(10, 'Check kembali cover back dan pocked, harus terpasang dengan benar.'),
(11, 'Check kembali klip back dan hogring, harus terpasang dengan benar dan tidak lepas.'),
(12, 'Check kembali cover recleaning outer dan handle recleaning, harus terpasang dengan benar dan tidak l'),
(13, 'Check kembali cover back dan cashion, harus sesuai spect dan standar keriput.'),
(14, 'Check kembali lumbar adjuster, harus terpasang dengan benar.'),
(15, 'Check kembali handle seat track, harus terpasang dengan kuat dan benar.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `solusi_pertanyaan_jok_tengah_seat_rear_satu`
--

CREATE TABLE IF NOT EXISTS `solusi_pertanyaan_jok_tengah_seat_rear_satu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `solusi` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data untuk tabel `solusi_pertanyaan_jok_tengah_seat_rear_satu`
--

INSERT INTO `solusi_pertanyaan_jok_tengah_seat_rear_satu` (`id`, `solusi`) VALUES
(1, 'Check kembali kanban, harus sesuai dengan model seat.'),
(2, 'Check kembali bagian belakang dan hanger, hanger tudak boleh lepas.'),
(3, 'Check kembali label information, harus rapih dan tidak lepas.'),
(4, 'Check kembali cover recleaning inner, harus terpasang dengan benar dan tidak lepas.'),
(5, 'Check kembali cover recleaning outer dan handle recleaning, harus terpasang dengan benar dan tidak l'),
(6, 'Check kembali handle release, harus terpasang dengan benar dan tidak lepas.'),
(7, 'Check kembali handle lock, harus terpasang dengan benar dan tidak oblak.'),
(8, 'Check kembali cover lock outer, harus terpasang dengan kencang dan benar.'),
(9, 'Check kembali nut hinge outer, harus terpasang dengan kencang dan benar.'),
(10, 'Check kembali cover lock inner, harus terpasang dengan kencang dan benar.'),
(11, 'Check kembali nut hinge inner, harus terpasang dengan kencang dan benar.'),
(12, 'Check kembali under cover/karpet, harus rapih dan sesuai sample yang terpasang.'),
(13, 'Check kembali head rest, harus rapi, tidak seret dan loss.'),
(14, 'Check kembali support head rest, harus terpasang dengan benar dan tidak terjepit.'),
(15, 'Check kembali seat belt, harus terpasang dengan benar dan sesuai spect.'),
(16, 'Check kembali cover back dan cashion, harus sesuai spect dan standar keriput.'),
(17, 'Check kembali handle seat track, harus terpasang dengan kuat dan benar.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `solusi_pertanyaan_sewing_jok_belakang_seat_rear_2`
--

CREATE TABLE IF NOT EXISTS `solusi_pertanyaan_sewing_jok_belakang_seat_rear_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `solusi` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `solusi_pertanyaan_sewing_jok_belakang_seat_rear_2`
--

INSERT INTO `solusi_pertanyaan_sewing_jok_belakang_seat_rear_2` (`id`, `solusi`) VALUES
(1, 'cek kembali kanbah harus sesuai dengan model');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
